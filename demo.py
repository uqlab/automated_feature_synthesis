from pydbml import PyDBML
from configparser import ConfigParser
import featuretools as ft
import sys
import glob
import os
import numpy as np
import pandas as pd

def read_tables_from_schema(parsed, data_path):
    df_dict = {}
    table_list = []
    for t in parsed.tables:
        table_list.append(t.name)

    for t in table_list:
        print(t)
        table_name = (data_path + t + '.csv')
        pd_df = pd.read_csv(table_name)
        df_dict[t] = (pd_df, pd_df.columns[0])
        print('-----Done reading ' + t + ' table')
    return df_dict


def read_realtions_from_schema(parsed):
    ref_list = []
    for r in parsed.refs:
        # print(r)
        if r.type == '<':
            parent = r.table1.name
            parent_ref = r.col1[0].name
            child = r.table2.name
            child_ref = r.col2[0].name
            x = (parent, parent_ref, child, child_ref)
            ref_list.append(x)  
    print('-----Done reading relationships')    
    return ref_list

def apply_featuretools(table_data, table_relationships, target_table):
    feature_matrix, _ = ft.dfs(dataframes=table_data,
                               relationships=table_relationships,
                               target_dataframe_name=target_table)
    return feature_matrix


def get_features():
        
    con_parser = ConfigParser()
    con_parser.read('configurations.ini')
        
    data_path = con_parser.get('paths', 'table_data_path')
    schema_path = con_parser.get('paths', 'table_schema_path')
    target_path = con_parser.get('paths', 'generated_feat_path')
    
    schema_name = con_parser.get('file_names', 'schema_name')
#     target_date = con_parser.get('targets', 'target_date')
    target_table = con_parser.get('targets', 'target_table')
    
    with open(schema_path + schema_name) as f:
        parsed = PyDBML(f)
        
    table_data = read_tables_from_schema(parsed, data_path)
#     print(table_data)
    table_relationships = read_realtions_from_schema(parsed)
#     print(table_relationships)

    features = apply_featuretools(table_data, table_relationships, target_table)
    print('-----Done extracting features')
    
    features.to_csv(target_path + target_table + '_features.csv', index=None)
    print('-----Done saving features')

if __name__ == '__main__':
    get_features()