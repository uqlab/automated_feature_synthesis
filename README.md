## Automated Feature Synthesis
This automated framework is an easy-to-access alternative to the traditional SQL queries on relational data. Namely, this package will take the database schema, data location and entity-relationship model, and automatically generate features by leveraging a collection of feature primitives that are type specific.


### Requirements
This repository contains three folders, one configuration (.ini) file and one executable python (.py) file

1. **data_table/**: This folder contains all the data table in .csv format. It is to note that all the tables should have an unique index column positioned as first column. 

#for example a customers.csv table may look like this
```
|-- table-name: customers.csv
|-- columns: customer_id(unique), customer_name, address
```

2. **schema/**: This folder contains the table schemas and entity-relationship in .dbml format. 

#for example a table_schema.dbml with customers table and  transactions table may look like this
```
Table "customers" {
  "customer_id" integer
  "customer_name" object
  "address" datetime64
}

Table "transactions" {
  "transaction_id" integer
  "customer_id" integer
  "transaction_time" datetime64
  "amount" float64  
}

Ref customer_transaction: "customers"."customer_id" < "transactions"."customer_id"
```

3. **gen_feat/**: This folder contains the generated features in .csv format file.

4. **configurations.ini**: This configuration file contains the data location (for example: 'data_table/' folder), .dbml schema location, .dbml schema name etc. that the executable file would read automatically. It also contains the target table name that we want to generate feature for. Namely, a user will make change in this configuration file based on their requirements and the executable file will read them automatically from this file.


#for example a configurations.ini file may look like this for this
```
[targets]
target_table = customers

[paths]
table_data_path = data_table/
table_schema_path = schema/
generated_feat_path = gen_feat/

[file_names]
schema_name = table_schema.dbml
```

5. **demo.py**: This is the executable python script that a user needs to run after making required change in the configurations.ini file.


## RUN

1. Step 1: make change in the **configurations.ini** file according to data table and schema locations

2. Step 2: execute the **demo.py** file

3. Step 3: go to **gen_feat/** folder to get the generated feature file in .csv format